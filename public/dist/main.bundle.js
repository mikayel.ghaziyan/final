webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/add-question/add-question.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/add-question/add-question.component.html":
/***/ (function(module, exports) {

module.exports = "<a href=\"\" (click)=\"logout()\">Logout</a><br><br>\n<a href=\"\" [routerLink]=\"['']\">Home</a>\n\n<h1>New Question</h1>\n<form (submit)=\"createQuestions()\" #questionForm = 'ngForm'>\n\tQuestion:<br> <textarea rows=\"4\" cols=\"50\" name=\"question\" [(ngModel)]=\"newQuestion.question\"></textarea><br>\n\t<!-- <p *ngIf=\"question.invalid && question.touched\" [ngStyle]='{\"color\":\"red\"}'>The Question is required</p> -->\n\tDescription (optional): <br><textarea rows=\"4\" cols=\"50\" autofocus name=\"description\" [(ngModel)]=\"newQuestion.description\"></textarea><br>\n\t<input type=\"button\" (click)=\"cancel()\" value=\"Cancel\"> \n\t<input type=\"submit\" value=\"Submit\" [disabled] = 'questionForm.invalid'> \n</form>\n\n"

/***/ }),

/***/ "../../../../../src/app/add-question/add-question.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var AddQuestionComponent = /** @class */ (function () {
    function AddQuestionComponent(_apiService, _router) {
        this._apiService = _apiService;
        this._router = _router;
        this.newQuestion = { question: '', description: '' };
    }
    AddQuestionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._apiService.getCurrentUser();
        this._apiService.currentUserData.subscribe(function (dataFromService) {
            if (dataFromService == null) {
                _this._router.navigate(['login']);
            }
            else {
                // this.current_user = dataFromService;
                _this._router.navigate(['addQuestion']);
            }
        });
    };
    AddQuestionComponent.prototype.createQuestions = function () {
        this._apiService.createQuestions(this.newQuestion);
        this.newQuestion = { question: '', description: '' };
        this._router.navigate(['']);
    };
    AddQuestionComponent.prototype.cancel = function () {
        this.newQuestion = { question: '', description: '' };
        this._router.navigate(['addQuestion']);
    };
    AddQuestionComponent.prototype.logout = function () {
        this._apiService.logout();
    };
    AddQuestionComponent = __decorate([
        core_1.Component({
            selector: 'app-add-question',
            template: __webpack_require__("../../../../../src/app/add-question/add-question.component.html"),
            styles: [__webpack_require__("../../../../../src/app/add-question/add-question.component.css")]
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService,
            router_1.Router])
    ], AddQuestionComponent);
    return AddQuestionComponent;
}());
exports.AddQuestionComponent = AddQuestionComponent;


/***/ }),

/***/ "../../../../../src/app/answer-question/answer-question.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/answer-question/answer-question.component.html":
/***/ (function(module, exports) {

module.exports = "<a href=\"\" (click)=\"logout()\">Logout</a><br><br>\n<a href=\"\" [routerLink]=\"['']\">Home</a>\n\n<h1>{{theOneQuesiton.question}}</h1>\n\n<h3>{{theOneQuesiton.description}}</h3>\n\n<h1>Your Answer</h1>\n<form (submit)=\"createAnswer()\" #answerForm = 'ngForm'>\n\tQuestion:<br> <textarea rows=\"4\" cols=\"50\" name=\"answer\" [(ngModel)]=\"newAnswer.answer\"></textarea><br>\n\t<!-- <p *ngIf=\"question.invalid && question.touched\" [ngStyle]='{\"color\":\"red\"}'>The Question is required</p> -->\n\tSupporting Details for you answer (optional): <br><textarea rows=\"4\" cols=\"50\" autofocus name=\"details\" [(ngModel)]=\"newAnswer.details\"></textarea><br>\n\t<input type=\"button\" (click)=\"cancel()\" value=\"Cancel\"> \n\t<input type=\"submit\" value=\"Submit\" [disabled] = 'answerForm.invalid'> \n</form>\n"

/***/ }),

/***/ "../../../../../src/app/answer-question/answer-question.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var AnswerQuestionComponent = /** @class */ (function () {
    function AnswerQuestionComponent(_route, _router, _apiService) {
        this._route = _route;
        this._router = _router;
        this._apiService = _apiService;
        this.id = { question_id: 0 };
        this.newAnswer = { answer: '', details: '', question_id: '' };
    }
    AnswerQuestionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.params.subscribe(function (params) {
            return _this.id.question_id = params['id'];
        });
        // get the quesiton
        this._apiService.getOneQuestion(this.id);
        this._apiService.oneQuestion.subscribe(function (dataFromService) {
            _this.theOneQuesiton = dataFromService[0];
        });
        // theOneQuesiton: any = {question: '', description: ''};
    };
    AnswerQuestionComponent.prototype.createAnswer = function () {
        this.newAnswer.question_id = this.theOneQuesiton._id;
        this._apiService.createAnswer(this.newAnswer);
        this.newAnswer = { answer: '', details: '' };
        this._router.navigate(['']);
    };
    AnswerQuestionComponent.prototype.cancel = function () {
        this.newAnswer = { answer: '', details: '' };
        this._router.navigate(['addQuestion']);
    };
    AnswerQuestionComponent.prototype.logout = function () {
        this._apiService.logout();
    };
    AnswerQuestionComponent = __decorate([
        core_1.Component({
            selector: 'app-answer-question',
            template: __webpack_require__("../../../../../src/app/answer-question/answer-question.component.html"),
            styles: [__webpack_require__("../../../../../src/app/answer-question/answer-question.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router,
            api_service_1.ApiService])
    ], AnswerQuestionComponent);
    return AnswerQuestionComponent;
}());
exports.AnswerQuestionComponent = AnswerQuestionComponent;


/***/ }),

/***/ "../../../../../src/app/api.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var rxjs_1 = __webpack_require__("../../../../rxjs/Rx.js");
var ApiService = /** @class */ (function () {
    function ApiService(_http) {
        this._http = _http;
        this.userData = new rxjs_1.BehaviorSubject({});
        this.currentUserData = new rxjs_1.BehaviorSubject({});
        this.questions = new rxjs_1.BehaviorSubject({});
        this.oneQuestion = new rxjs_1.BehaviorSubject({});
        this.answers = new rxjs_1.BehaviorSubject({});
        this.getAllQuestions();
        this.getCurrentUser();
    }
    ApiService.prototype.getCurrentUser = function () {
        var _this = this;
        this._http.get('/api/current_user')
            .subscribe(function (responseData) {
            _this.currentUserData.next(responseData);
        });
    };
    ApiService.prototype.loginUser = function (login_user) {
        var _this = this;
        this._http.post('/api/login_user', login_user)
            .subscribe(function (responseData) {
            _this.userData.next(responseData);
        });
    };
    ApiService.prototype.createQuestions = function (new_question) {
        var _this = this;
        this._http.post('/api/create_question', new_question)
            .subscribe(function (responseData) {
            _this.getAllQuestions();
        });
    };
    ApiService.prototype.getAllQuestions = function () {
        var _this = this;
        this._http.get('/api/get_questions')
            .subscribe(function (responseData) {
            _this.questions.next(responseData);
        });
    };
    ApiService.prototype.getOneQuestion = function (question_id) {
        var _this = this;
        this._http.post('/api/get_one_question', question_id)
            .subscribe(function (responseData) {
            _this.oneQuestion.next(responseData);
        });
    };
    ApiService.prototype.createAnswer = function (new_answer) {
        this._http.post('/api/create_answer', new_answer)
            .subscribe(function (responseData) {
            // this.getAllAnswers();
            console.log('in the service I got answer for a question which is ', responseData);
        });
    };
    ApiService.prototype.getAnswers = function (question_id) {
        var _this = this;
        this._http.post('/api/get_answers', question_id)
            .subscribe(function (responseData) {
            _this.answers.next(responseData);
        });
    };
    ApiService.prototype.logout = function () {
        this._http.get('/api/logout')
            .subscribe(function (responseData) {
            console.log(responseData);
        });
    };
    ApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;


/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var login_component_1 = __webpack_require__("../../../../../src/app/login/login.component.ts");
var home_component_1 = __webpack_require__("../../../../../src/app/home/home.component.ts");
var add_question_component_1 = __webpack_require__("../../../../../src/app/add-question/add-question.component.ts");
var show_question_component_1 = __webpack_require__("../../../../../src/app/show-question/show-question.component.ts");
var answer_question_component_1 = __webpack_require__("../../../../../src/app/answer-question/answer-question.component.ts");
var routes = [
    { path: 'login', pathMatch: 'full', component: login_component_1.LoginComponent },
    { path: '', pathMatch: 'full', component: home_component_1.HomeComponent },
    { path: 'addQuestion', component: add_question_component_1.AddQuestionComponent },
    { path: 'answerQuestion/:id', component: answer_question_component_1.AnswerQuestionComponent },
    { path: 'showQuestion/:id', component: show_question_component_1.ShowQuestionComponent },
    { path: '**', component: home_component_1.HomeComponent },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var app_routing_module_1 = __webpack_require__("../../../../../src/app/app-routing.module.ts");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var home_component_1 = __webpack_require__("../../../../../src/app/home/home.component.ts");
var login_component_1 = __webpack_require__("../../../../../src/app/login/login.component.ts");
var edit_component_1 = __webpack_require__("../../../../../src/app/edit/edit.component.ts");
var add_question_component_1 = __webpack_require__("../../../../../src/app/add-question/add-question.component.ts");
var answer_question_component_1 = __webpack_require__("../../../../../src/app/answer-question/answer-question.component.ts");
var show_question_component_1 = __webpack_require__("../../../../../src/app/show-question/show-question.component.ts");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                login_component_1.LoginComponent,
                edit_component_1.EditComponent,
                add_question_component_1.AddQuestionComponent,
                answer_question_component_1.AnswerQuestionComponent,
                show_question_component_1.ShowQuestionComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                forms_1.FormsModule,
                http_1.HttpClientModule
            ],
            providers: [api_service_1.ApiService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".horizon {\r\n\tdisplay: inline-block;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<h3>Edit {{oneItem.item}}</h3>\n\n<form (submit)=\"editItems()\" #editItemForm = 'ngForm'> \n\t    <p class=\"horizon\">*Update Item: <input type=\"text\" name=\"item\" [(ngModel)]=\"editItem.item\" required #item = 'ngModel' placeholder=\"{{oneItem.item}}\"></p>\n\t    <p class=\"horizon\" *ngIf=\"item.invalid && item.touched\" [ngStyle]='{\"color\":\"red\"}'>Inuput is required</p>\n\t   \t<br>\n\t   <input type=\"submit\" value=\"Create Item\" [disabled]='editItemForm.invalid'>\t\t \n</form>"

/***/ }),

/***/ "../../../../../src/app/edit/edit.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var EditComponent = /** @class */ (function () {
    function EditComponent() {
    }
    // id: any;
    // oneItem: any;
    // editItem: any = {item: '', id: 0};
    // listItems: any;
    // constructor(
    //   private _route: ActivatedRoute,
    //   private _router: Router,
    //   private _apiService: ApiService, 
    //             ) { }
    EditComponent.prototype.ngOnInit = function () {
        //   // ITEMS
        //   this._apiService.items.subscribe((dataFromService:any)=>{ 
        //   this.listItems = dataFromService;
        //   })
        // 	this._route.params.subscribe((params: Params) => 
        // 	this.id = params['id']);
        // 	for (let item in this.listItems) {
        // 		if (this.listItems[item]._id == this.id) {
        // 			this.oneItem = this.listItems[item];
        // 		}
        // 	}
    };
    EditComponent = __decorate([
        core_1.Component({
            selector: 'app-edit',
            template: __webpack_require__("../../../../../src/app/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/edit/edit.component.css")]
        })
    ], EditComponent);
    return EditComponent;
}());
exports.EditComponent = EditComponent;


/***/ }),

/***/ "../../../../../src/app/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Hello {{current_user}}</h2>\n\n<a href=\"\" (click)=\"logout()\">Logout</a><br>\n<a href=\"\" [routerLink]=\"['addQuestion']\">Add a questions</a><br><hr>\n\n\n<div class=\"table\" >\n\t<table border=\"1\">\n\t\t<tr>\n\t\t\t<th>Questions</th>\n\t\t\t<th>Answers</th>\n\t\t\t<th>Action</th>\n\t\t</tr>\n\t    <tr *ngFor='let oneQuestion of listQuesitons'>\n\t \t\t<td>{{oneQuestion.question}}</td>\n\t \t\t<td>0</td>\n\t \t\t<td><a href=\"\" [routerLink]=\"['showQuestion', oneQuestion._id]\">Show</a> | \n\t \t\t\t<a href=\"\" [routerLink]=\"['answerQuestion', oneQuestion._id]\">Answer</a></td>\n\t    </tr>\n\t</table>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/home/home.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(_apiService, _router) {
        this._apiService = _apiService;
        this._router = _router;
        this.item_id = { id: 0 };
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._apiService.getCurrentUser();
        this._apiService.currentUserData.subscribe(function (dataFromService) {
            // Checking the current user
            if (dataFromService == null) {
                _this._router.navigate(['login']);
            }
            else {
                _this.current_user = dataFromService;
                _this._router.navigate(['']);
            }
        });
        // getting questions
        this._apiService.getAllQuestions();
        this._apiService.questions.subscribe(function (dataFromService) {
            _this.listQuesitons = dataFromService;
        });
    };
    // deleteItem(id){
    //   this.item_id.id = id;
    //   this._apiService.deleteItem(this.item_id);
    //   this.item_id = {id: 0};
    //   this._router.navigate(['']);
    // }
    HomeComponent.prototype.logout = function () {
        this._apiService.logout();
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService,
            router_1.Router])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;


/***/ }),

/***/ "../../../../../src/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n#loginReg {\r\n\tbackground:\t#F0F8FF;\r\n\tmargin-top: 10px;\r\n\tborder: 2px solid black;\r\n\tpadding: auto;\r\n\ttext-align: center;\r\n}\r\n\r\n.logRegForms {\t\r\n\tmargin: 30px 30px 30px 30px;\r\n\tpadding: 5px;\r\n\tborder: 1px solid gray;\r\n\tdisplay: inline-block;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Q and A</h1>\n\n<div id=\"loginReg\">\n<!-- login -->\n\t<form class=\"logRegForms\" (submit)=\"login()\" #loginForm = 'ngForm'>\n\t    <p>*Name: <input type=\"text\" name=\"loginName\" [(ngModel)]=\"login_user.name\"\n\t    required #loginName = \"ngModel\"></p>\n\t    <p *ngIf=\"loginName.invalid && loginName.touched\" [ngStyle]='{\"color\":\"red\"}'>The Name is required</p>\n\t   <p><input type=\"submit\" value=\"Log in\" [disabled] = 'loginForm.invalid'></p>\t\t \n\t</form>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(_apiService, _router) {
        this._apiService = _apiService;
        this._router = _router;
        this.login_user = { name: '' };
        this.login_error = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this._apiService.loginUser(this.login_user);
        this._apiService.userData.subscribe(function (dataFromService) {
            if ((dataFromService == _this.login_user.name)) {
                // no error while loggin in
                _this._router.navigate(['']);
            }
            else {
                _this.login_error = 'Something went wrong. Please try again';
                _this._router.navigate(['login']);
            }
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "../../../../../src/app/show-question/show-question.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/show-question/show-question.component.html":
/***/ (function(module, exports) {

module.exports = "<a href=\"\" (click)=\"logout()\">Logout</a><br><br>\n<a href=\"\" [routerLink]=\"['']\">Home</a>\n\n<h1>{{theOneQuesiton.question}}</h1>\n\n<h3>{{theOneQuesiton.description}}</h3>\n\n<h4>Answers</h4>\n<hr>\n<h3 *ngFor=\"let answer of listAnswers\">{{answer.answer}}</h3>"

/***/ }),

/***/ "../../../../../src/app/show-question/show-question.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var ShowQuestionComponent = /** @class */ (function () {
    function ShowQuestionComponent(_route, _router, _apiService) {
        this._route = _route;
        this._router = _router;
        this._apiService = _apiService;
        this.id = { question_id: 0 };
        this.newAnswer = { answer: '', details: '', question_id: '' };
    }
    ShowQuestionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.params.subscribe(function (params) {
            return _this.id.question_id = params['id'];
        });
        ;
        // get the quesiton
        this._apiService.getOneQuestion(this.id);
        this._apiService.oneQuestion.subscribe(function (dataFromService) {
            _this.theOneQuesiton = dataFromService[0];
        });
        //get all ansers
        this._apiService.getAnswers(this.id);
        this._apiService.answers.subscribe(function (dataFromService) {
            _this.listAnswers = dataFromService;
            console.log('answers in comp', _this.listAnswers);
        });
        // theOneQuesiton: any = {question: '', description: ''}; 
    };
    ShowQuestionComponent = __decorate([
        core_1.Component({
            selector: 'app-show-question',
            template: __webpack_require__("../../../../../src/app/show-question/show-question.component.html"),
            styles: [__webpack_require__("../../../../../src/app/show-question/show-question.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router,
            api_service_1.ApiService])
    ], ShowQuestionComponent);
    return ShowQuestionComponent;
}());
exports.ShowQuestionComponent = ShowQuestionComponent;


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map