import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ApiService {
 
 userData: BehaviorSubject<object> = new BehaviorSubject({});
 currentUserData: BehaviorSubject<object> = new BehaviorSubject({});
 questions: BehaviorSubject<object> = new BehaviorSubject({});
 oneQuestion: BehaviorSubject<object> = new BehaviorSubject({});
 answers: BehaviorSubject<object> = new BehaviorSubject({});

  constructor(private _http: HttpClient) {
    this.getAllQuestions();
    this.getCurrentUser();
   }

  getCurrentUser() {
  	this._http.get('/api/current_user')
  	.subscribe((responseData:any)=>{
  		this.currentUserData.next(responseData);
  	})
  }

   loginUser(login_user) {
  	this._http.post('/api/login_user', login_user)
  	.subscribe((responseData:any)=>{
  		this.userData.next(responseData);
  	})
  }

  createQuestions(new_question) {
    this._http.post('/api/create_question', new_question)
    .subscribe((responseData:any)=>{ 
       this.getAllQuestions();
    })
  }

  getAllQuestions() {
    this._http.get('/api/get_questions')
    .subscribe((responseData:any)=>{
      this.questions.next(responseData);
    })
  }

  getOneQuestion(question_id){
    this._http.post('/api/get_one_question', question_id)
    .subscribe((responseData:any)=>{
      this.oneQuestion.next(responseData);
      
    })
  }

  createAnswer(new_answer) {
    this._http.post('/api/create_answer', new_answer)
    .subscribe((responseData:any)=>{ 
       // this.getAllAnswers();
       console.log('in the service I got answer for a question which is ', responseData);
    })
  }

  getAnswers(question_id){
    
    this._http.post('/api/get_answers', question_id)
    .subscribe((responseData:any)=>{
      this.answers.next(responseData);  
    })
  }

  logout(){
  	this._http.get('/api/logout')
  	.subscribe((responseData:any)=>{
  		console.log(responseData);
  	})
  }
}
