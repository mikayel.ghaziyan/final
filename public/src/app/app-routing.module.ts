import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { AddQuestionComponent } from './add-question/add-question.component';
import { ShowQuestionComponent } from './show-question/show-question.component';
import { AnswerQuestionComponent } from './answer-question/answer-question.component';
const routes: Routes = [
{ path: 'login', pathMatch: 'full', component: LoginComponent },
{ path: '', pathMatch: 'full', component: HomeComponent },
{ path: 'addQuestion', component: AddQuestionComponent},
{ path: 'answerQuestion/:id', component: AnswerQuestionComponent},
{ path: 'showQuestion/:id', component: ShowQuestionComponent},
{ path: '**', component: HomeComponent }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
