import { Component, OnInit } from '@angular/core';
import { ApiService } from './../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

 login_user = {name: ''};

  login_error: string = '';

  constructor(private _apiService: ApiService, private _router: Router) { }

   ngOnInit() {
  
   }
 
  login(){

		this._apiService.loginUser(this.login_user);
		
		this._apiService.userData.subscribe((dataFromService:any)=>{

			if ((dataFromService == this.login_user.name)){						
				// no error while loggin in
				this._router.navigate(['']);
			}
			else  {
				this.login_error = 'Something went wrong. Please try again';	
				this._router.navigate(['login']);
			}
		})				
  }
}
