import { Component, OnInit } from '@angular/core';
import { ApiService } from './../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {

  newQuestion: any = {question: '', description: ''};

  constructor(private _apiService: ApiService, 
              private _router: Router) { }

  ngOnInit() {
  	  this._apiService.getCurrentUser();
  	this._apiService.currentUserData.subscribe((dataFromService:any)=>{
		
		if(dataFromService == null){
			this._router.navigate(['login']);
		} else {
			// this.current_user = dataFromService;
			this._router.navigate(['addQuestion']);
		  }
	  })
  }

  createQuestions() {

    this._apiService.createQuestions(this.newQuestion);
    this.newQuestion = {question: '', description: ''};
    this._router.navigate(['']);
  }

  cancel(){
  	this.newQuestion = {question: '', description: ''};
  	this._router.navigate(['addQuestion']);
  }

   logout(){
    this._apiService.logout();
  }
}
