import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ApiService } from './../api.service';

@Component({
  selector: 'app-show-question',
  templateUrl: './show-question.component.html',
  styleUrls: ['./show-question.component.css']
})
export class ShowQuestionComponent implements OnInit {
  listAnswers: any;
  theOneQuesiton: any;
  id: any = {question_id: 0};
  newAnswer: any = {answer: '', details: '', question_id: ''};

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _apiService: ApiService) { }

  ngOnInit() {

  	this._route.params.subscribe((params: Params) => 
  	this.id.question_id = params['id']));
  	// get the quesiton
    this._apiService.getOneQuestion(this.id);
    this._apiService.oneQuestion.subscribe((dataFromService:any)=>{ 
    this.theOneQuesiton = dataFromService[0]; })
	//get all ansers
	  this._apiService.getAnswers(this.id);
    this._apiService.answers.subscribe((dataFromService:any)=>{ 
    this.listAnswers = dataFromService;
    console.log('answers in comp', this.listAnswers);
    })	
    // theOneQuesiton: any = {question: '', description: ''}; 

  }

}
