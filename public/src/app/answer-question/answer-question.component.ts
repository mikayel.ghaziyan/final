import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ApiService } from './../api.service';

@Component({
  selector: 'app-answer-question',
  templateUrl: './answer-question.component.html',
  styleUrls: ['./answer-question.component.css']
})
export class AnswerQuestionComponent implements OnInit {

  theOneQuesiton: any;
  id: any = {question_id: 0};
  newAnswer: any = {answer: '', details: '', question_id: ''};

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _apiService: ApiService) { }

  ngOnInit() {

  	this._route.params.subscribe((params: Params) => 
  	this.id.question_id = params['id']);
  	// get the quesiton
    this._apiService.getOneQuestion(this.id);
    this._apiService.oneQuestion.subscribe((dataFromService:any)=>{ 
    this.theOneQuesiton = dataFromService[0];
    })	
    // theOneQuesiton: any = {question: '', description: ''};
  }

  createAnswer() {
  	this.newAnswer.question_id = this.theOneQuesiton._id;
    this._apiService.createAnswer(this.newAnswer);
    this.newAnswer = {answer: '', details: ''};
    this._router.navigate(['']);
  }

  cancel(){
  	this.newAnswer = {answer: '', details: ''};
  	this._router.navigate(['addQuestion']);
  }

   logout(){
    this._apiService.logout();
  }
}
