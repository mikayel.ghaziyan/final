import { Component, OnInit } from '@angular/core';
import { ApiService } from './../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  current_user: any;
  listQuesitons: any; 
  item_id = {id: 0};
  
  constructor(private _apiService: ApiService, 
              private _router: Router) { }

  ngOnInit() {

    this._apiService.getCurrentUser();
  	this._apiService.currentUserData.subscribe((dataFromService:any)=>{
		
		// Checking the current user
		if(dataFromService == null){
			this._router.navigate(['login']);
		} else {
			this.current_user = dataFromService;
			this._router.navigate(['']);
		  }
	  })

    // getting questions
    this._apiService.getAllQuestions();
    this._apiService.questions.subscribe((dataFromService:any)=>{ 
    this.listQuesitons = dataFromService;
    })
  }


  // deleteItem(id){
  //   this.item_id.id = id;
  //   this._apiService.deleteItem(this.item_id);
  //   this.item_id = {id: 0};
  //   this._router.navigate(['']);
  // }

   logout(){
    this._apiService.logout();
  }
}
