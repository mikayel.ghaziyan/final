// 1. Load modules into variables
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var session = require('express-session');

// 2. Configure express
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public', 'dist')));
// app.use(express.static(__dirname + '/angular-app/dist'));
app.use(session({secret:'SuperSecret'}));

// 3. Mongoose connection
require('./server/config/mongoose.js');

// 4. Load routes
var routes_setter = require('./server/config/routes.js');
routes_setter(app);

// 5. Server listener
app.listen(8000, function(){
  console.log('on port 8000');
})