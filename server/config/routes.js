var Users = require('./../controllers/users.js');
var Items = require('./../controllers/items.js');
var path = require('path');
module.exports = function(app) {
	// USERS
	app.get('/api/current_user', Users.current);
	app.post('/api/login_user', Users.login);
	app.get('/api/logout', Users.logout);
	
	// Questions
	app.post('/api/create_question', Items.create_question);
	app.get('/api/get_questions', Items.getQuestions);
	app.post('/api/get_one_question', Items.getOneQuestion);
	app.post('/api/create_answer', Items.create_answer);

	// Answers
	app.post('/api/get_answers', Items.getAnswers);

	// GENERAL
	app.all("**", (request, response) => { response.sendFile(path.resolve("./public/dist/index.html")) });
}

