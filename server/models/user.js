var mongoose = require('mongoose');
var validate = require('mongoose-validator');
	
  /// Question
  // validator
var questionValidator = [
  validate({
    validator: 'isLength',
    arguments: [10, 150],
    message: 'The question should be between {ARGS[0]} and {ARGS[1]} characters'
  })];


var ItemSchema = new mongoose.Schema({
  question: {type: String, required: true, validate: questionValidator},
  description: {type: String}
})

mongoose.model('Item', ItemSchema);

/// Answers

var answerValidator = [
  validate({
    validator: 'isLength',
    arguments: [5, 150],
    message: 'The answer should be between {ARGS[0]} and {ARGS[1]} characters'
  })];


var AnswersSchema = new mongoose.Schema({
  answer: {type: String, required: true, validate: answerValidator},
  details: {type: String},
  question_id: {type: String}
})

mongoose.model('Answers', AnswersSchema);