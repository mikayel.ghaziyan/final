var mongoose = require('mongoose');
var Item = mongoose.model('Item');
var Answers = mongoose.model('Answers');

module.exports = {

	getQuestions: function(req, res) {
		Item.find({}, function(err, allQuestions){
			if(err){
				console.log('Something went wrong');
				res.json(err);
			}
			else{
				res.json(allQuestions);
			}
		})
	},

	getOneQuestion: function(req, res) {
		Item.find({_id: req.body.question_id}, function(err, theQuestion){
			if(err){
				console.log('Something went wrong');
				res.json(err);
			}
			else{
				res.json(theQuestion);
			}
		})
	},

	create_question: function(req, res) {
		
		var newQuestion = new Item(req.body);
		
		newQuestion.save(function(err){
		
			if(err){
				res.json(err);
			}
			else {

			res.json('Question was created');
			}
		})
	},

	create_answer: function(req, res) {

		var newAnswer = new Answers(req.body);
		
		newAnswer.save(function(err){
		
			if(err){
				res.json(err);
			}
			else {
			res.json('Answer was created');
			}
		})
	},
	getAnswers: function(req, res) {
		console.log('in the server', req.body.question_id);
		Answers.find({question_id: req.body.question_id}, function(err, allAnswers){
			if(err){
				console.log('Something went wrong');
				res.json(err);
			}
			else{
				res.json(allAnswers);
			}
		})
	},
}