var mongoose = require('mongoose');

module.exports = {

	index: function(req, res) {
		User.find({}, function(err, allUsers){
			if(err){
				console.log('Something went wrong');
				res.json(err);
			}
			else{
				res.json(allUsers);
			}
		})
	},

	current: function(req, res) { 
		if(req.session.name == undefined){
			res.json(null);
		} else {
			res.json(req.session.name);
		}
	},

	login: function(req, res) { 
		req.session.name = req.body.name;
		res.json(req.session.name);
	},

	logout: function (req, res) {
		console.log('it still existing ', req.session.userId);
		req.session.destroy();
		res.json('logged out session destroyed');
	}
}